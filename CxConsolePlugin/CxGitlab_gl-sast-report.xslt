<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/TR/xhtml1/strict" version="1.0">
   <xsl:output method="xml" indent="yes" encoding="iso-8859-1" />
   <xsl:template match="/">
[
      <xsl:for-each select="CxXMLResults">
         <xsl:for-each select="Query[@Severity='High']">
            <xsl:sort select="@Severity" data-type="text" order="ascending" />
            <xsl:sort select="@name" data-type="text" order="ascending" />
            <xsl:for-each select="Result">
{
"category": "sast",
"name": "<xsl:value-of select="../@name" />",
"message": "<xsl:value-of select="../@name" />",
"description": "<xsl:value-of select="../@DeepLink" />",
"cve": "",
"scanner": {"id": "CxSAST","name": "Checkmarx CxSAST"},
"location": {"file": "<xsl:for-each select="@FileName"><xsl:value-of select="string(.)" /></xsl:for-each>", "start_line":<xsl:for-each select="@Line"><xsl:value-of select="string(.)" /></xsl:for-each>},
"identifiers": [],
"file": "<xsl:for-each select="@FileName"><xsl:value-of select="string(.)" /></xsl:for-each>",
"line":<xsl:for-each select="@Line"><xsl:value-of select="string(.)" /></xsl:for-each>,
"tool": "CxSAST"
},
            </xsl:for-each>
         </xsl:for-each>
      </xsl:for-each>
      <xsl:variable name="highResults" select="count(CxXMLResults/Query/Result[@state!=1 and @Severity='High'])" />
      <xsl:variable name="medResults" select="count(CxXMLResults/Query/Result[@state!=1 and @Severity='Medium'])" />
      <xsl:variable name="lowResults" select="count(CxXMLResults/Query/Result[@state!=1 and @Severity='Low'])" />
      <xsl:variable name="infoResults" select="count(CxXMLResults/Query/Result[@state!=1 and @Severity='Information'])" />
{
"category": "sast",
"name": "<xsl:value-of select="$highResults" /> High vulnerabilities detected in this project",
"message": "<xsl:value-of select="$highResults" /> High vulnerabilities detected in this project",
"description": "<xsl:value-of select="//Result[1]/@DeepLink" />",
"cve": "",
"scanner": {"id": "CxSAST","name": "Checkmarx CxSAST"},
"location": {"file": "","start_line":""},
"identifiers": [],
"file": "",
"line":"",
"tool": "CxSAST"
},
{
"category": "sast",
"name": "<xsl:value-of select="$medResults" /> Medium vulnerabilities detected in this project",
"message": "<xsl:value-of select="$medResults" /> Medium vulnerabilities detected in this project",
"description": "<xsl:value-of select="//Result[1]/@DeepLink" />",
"cve": "",
"scanner": {"id": "CxSAST","name": "Checkmarx CxSAST"},
"location": {"file": "","start_line":""},
"identifiers": [],
"file": "",
"line":"",
"tool": "CxSAST"
},
{
"category": "sast",
"name": "<xsl:value-of select="$lowResults" /> Low vulnerabilities detected in this project",
"message": "<xsl:value-of select="$lowResults" /> Low vulnerabilities detected in this project",
"description": "<xsl:value-of select="//Result[1]/@DeepLink" />",
"cve": "",
"scanner": {"id": "CxSAST","name": "Checkmarx CxSAST"},
"location": {"file": "","start_line":""},
"identifiers": [],
"file": "",
"line":"",
"tool": "CxSAST"
},
{
"category": "sast",
"name": "<xsl:value-of select="$infoResults" /> Informative items",
"message": "<xsl:value-of select="$infoResults" /> Informative items",
"description": "<xsl:value-of select="//Result[1]/@DeepLink" />",
"cve": "",
"scanner": {"id": "CxSAST","name": "Checkmarx CxSAST"},
"location": {"file": "","start_line":""},
"identifiers": [],
"file": "",
"line":"",
"tool": "CxSAST"
}
]
   </xsl:template>
</xsl:stylesheet>